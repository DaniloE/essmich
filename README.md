# Essmich App

## Installation

* Installation PostgreSQL (getestet mit 9.6)
   * Default für die Datenbank in settings.py:
   * User: essenskasse, PW: essenskasse, DB: essenskasse
   * DB und Nutzer entsprechend anlegen
   * ggf. pg_hba.conf anpassen

* Python3 virtenv
* Pakete zur Installation (auch in requirements.txt):
```
Django
django-bootstrap4
psycopg2-binary
Pillow
django-python3-ldap
```
* python manage.py migrate
* python manage.py createsuperuser

* Installation apache + mod_wsgi
* Konfiguration des Apache entsprechend dem Applikationsordner usw.

## Models

### Supplier
| Feld        | Erklärung   |
| ------------- |-------------|
| name | Name des Lieferanten|
| menu | ImageField für die Speisekarte|
| menulink | Textfeld für den Link zu Speisekarte|

### Orders
| Feld        | Erklärung   |
| ------------- |-------------|
| start_date | Wird automatisch gesetzt. Zeit zu der die Bestellung eröffnet wurde.|
| end_date |Zeit zu der die Bestellung beendet wird. Diese dient nur als Information auf der Bestelldetails-Seite
und wird standardmäßig auf Startzeit + 2 Stunden gesetzt. |
| supplier |FK-Beziehung zu dem Lieferanten, bei dem die Bestellung durchgeführt wird. |
| deliverytime |Voraussichtliche Lieferzeit. Nur als Information auf der Bestelldetails-Seite sichtbar, wenn die Bestellung abgeschlossen wird. |
| active  |Wenn True, ist die Bestellung offen und kann bearbeitet werden, wenn False geschlossen.|
| deactivate_date | Wird automatisch gesetzt wenn die Bestellung deaktiviert wird. Mechanismus um sicherzustellen, dass eine Bestellung nicht neu geöffnet wird, bzw. Preise nicht doppelt abgezogen werden.|

### Usersaldo
Erweitert die Use-Klasse von Django durch OneToOneField Beziehung
| Feld        | Erklärung   |
| ------------- |-------------|
| user |User-Fremdschlüssel |
| saldo |Aktueller Kassenstand des Nutzers |

### OrderItems

| Feld        | Erklärung   |
| ------------- |-------------|
| user |User-FK |
| original_value |OriginalPreis des Bestellpunktes |
| value |Preise nach eventuellen Rabattregeln|
| title |Bezeichnung des Bestellpunktes |
| ordernumber |Bestellnummer des Bestellpunktes |
|order | FK der Bestellung|

### DiscountRules
| Feld        | Erklärung   |
| ------------- |-------------|
| supplier |FK des Lieferanten |
| orderitems| RegEx der zu verarbeitenden Bestellnummern |
| discount_operator |Operator auf den Preis. via Choice-Set festgelegt auf +-x= |
| discount_value |Wert mit dem der ursprüngliche Preis via Operator verrechnet werden soll |

### Deposit
| Feld        | Erklärung   |
| ------------- |-------------|
| user | FK des Nutzers|
| value |Höhe der Gutschrift |
| accepted | Gutschrift azeptiert wenn True, default ist False bis zur Bestätigung im Admin-Bereich|
| accepted_date | Datum wann der Betrag bestätigt wurde |
| deposit_date | Datum wann die Gutschrift durch den Nutzer beantragt wurde|

## Hooks zu den Models

### modify_new_user
Erstellt den User-saldo Eintrag wenn ein neuer Nutzer erstellt wird

### add_deposit_to_user
Schreibt die Gutschrift dem Nutzer zu wenn diese im Backend bestätigt wurde und noch kein accepted_date vorliegt

### calculate_debits_for_user
Berechnet den neuen Nutzersaldo wenn eine Bestellung abgeschlossen wurde und noch kein deactivate_date vorliegt. 
Setzt das deactivate_date.

## Konfiguration nach Einrichtung

* Im Adminbackend die Supplier einrichten
* Bestellungen anlegen
