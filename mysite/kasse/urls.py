from django.conf import settings
from django.conf.urls.static import static
from django.urls import path

from . import views

app_name = 'kasse'
urlpatterns = [    
    path('', views.Index, name='index'),
    path('supplier/', views.Suppliers, name='supplier'),
    path('statistics/', views.Statistics, name='statistics'),
    path('myorders/', views.Myorders, name='myorders'),
    path('orders/<int:pk>/', views.Orderdetail, name='orderdetail'),
    path('accounting/', views.Accounting, name='accounting'),
    path('deposit/', views.deposit, name='deposit'),
    path('<int:order_id>/order/', views.order, name='order'),
    path('delete_orderitem/<int:orderitem_id>/<int:order_id>/', views.delete_orderitem, name='delete_orderitem'),
]