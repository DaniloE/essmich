from datetime import timedelta

from django.contrib.auth.models import User
from django.db import models
from django.db.models import F
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils import timezone
from django.core.mail import send_mail
from django.conf import settings

class Usersaldo(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    saldo = models.DecimalField(decimal_places=2, max_digits=5, default='0')    


class Supplier(models.Model):
    name = models.CharField(max_length=200)
    menu = models.ImageField(upload_to='menus', null=True, blank=True)
    menulink = models.CharField(max_length=300, default='')

    def __str__(self):
        return self.name


class Orders(models.Model):    
    end_time = timezone.now() + timedelta(hours=2)
    start_date = models.DateTimeField(default=timezone.now)
    end_date = models.DateTimeField(default=end_time)
    supplier = models.ForeignKey(Supplier, on_delete=models.CASCADE)
    deliverytime = models.DateTimeField(default=end_time)
    active = models.BooleanField(default=True)
    deactivate_date = models.DateTimeField(blank=True, null=True)

    def __str__(self):
        order_name = self.supplier.name + '_' + str(self.start_date)
        return order_name


class OrderItems(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    original_value = models.DecimalField(decimal_places=2, max_digits=5)
    value = models.DecimalField(decimal_places=2, max_digits=5)
    title = models.CharField(max_length=200)
    ordernumber = models.IntegerField(default=0)
    order = models.ForeignKey(Orders, on_delete=models.CASCADE)


class DiscountRules(models.Model):
    operator_choices = (('=', '='),
                        ('*', '*'),
                        ('+', '+'),
                        ('-', '-'))
    supplier = models.ForeignKey(Supplier, on_delete=models.CASCADE)
    orderitems = models.CharField(max_length=999)
    discount_operator = models.CharField(
        max_length=2, choices=operator_choices)
    discount_value = models.FloatField(default=0)

    def __str__(self):
        return self.supplier.name+str(self.id)


class Deposit(models.Model):    
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    value = models.DecimalField(decimal_places=2, max_digits=5)
    accepted = models.BooleanField(default=False)
    accepted_date = models.DateTimeField(blank=True, null=True)
    deposit_date = models.DateTimeField(default=timezone.now)

    def __str__(self):
        deposit_name = self.user.username + '_' + str(self.deposit_date)
        return deposit_name


@receiver(post_save, sender=User)
def modify_new_user(sender, instance, created, **kwargs):
    if created:
        try:
            Usersaldo.objects.create(user=instance)
        except Exception as e:
            print(e)


@receiver(post_save, sender=Deposit)
def add_deposit_to_user(sender, instance, **kwargs):    
    if instance.accepted_date is None and instance.accepted is True:
        try:
            Usersaldo.objects.filter(user=instance.user).update(
                saldo=F('saldo') + instance.value)
            Deposit.objects.filter(pk=instance.id).update(
                accepted_date=timezone.now())
        except Exception as e:
            print(e)


@receiver(post_save, sender=Orders)
def calculate_debits_for_user(sender, instance, **kwargs):    
    if instance.deactivate_date is None and not instance.active:
        for item in instance.orderitems_set.all():
            try:
                Usersaldo.objects.filter(user=item.user).update(
                    saldo=F('saldo') - item.value)
                Orders.objects.filter(pk=instance.id).update(
                    deactivate_date=timezone.now())
            except Exception as e:
                print(e)
