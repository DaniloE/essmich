from django.contrib.auth.decorators import login_required
from django.shortcuts import get_object_or_404, render
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib.auth.models import User
from django.db.models import Sum, Count
import re

from .models import Orders, Supplier, OrderItems, Deposit


@login_required
def order(request, order_id):
    order_id = order_id
    user = User.objects.get(username=request.user.username)
    title = request.POST.get("orderitem")
    ordernumber = request.POST.get("ordernumber")
    original_value = float(request.POST.get(
        "orderitem_value").replace(',', '.'))
    lorder = get_object_or_404(Orders, pk=order_id)
    discount_rules = lorder.supplier.discountrules_set.all()
    value = original_value
    for rule in discount_rules:
        items = rule.orderitems
        operator = rule.discount_operator
        discount_value = rule.discount_value
        if re.match(ordernumber, items):
            if operator == '+':
                value = value + discount_value
            elif operator == '-':
                value = value - discount_value
            elif operator == '*':
                value = value * discount_value
            elif operator == '=':
                value = discount_value
    try:
        lorder.orderitems_set.create(user=user, title=title, original_value=original_value,
                                     value=value, order=lorder.id, ordernumber=ordernumber)
    except Exception as e:
        print(e)
    return HttpResponseRedirect(reverse('kasse:orderdetail', args=(lorder.id,)))


@login_required
def delete_orderitem(request, orderitem_id, order_id):
    user = User.objects.get(username=request.user.username)
    try:
        OrderItems.objects.filter(id=orderitem_id).delete()
    except Exception as e:
        print(e)
    return HttpResponseRedirect(reverse('kasse:orderdetail', args=(order_id,)))


@login_required
def Index(request):
    order_list = Orders.objects.filter(active=True)
    user = User.objects.get(username=request.user.username)
    saldo = user.usersaldo.saldo
    context = {'order_list': order_list,
               'user': request.user.username,
               'saldo': str(saldo)}
    return render(request, 'kasse/index.html', context)


@login_required
def Suppliers(request):
    supplier_list = Supplier.objects.order_by('name')
    user = User.objects.get(username=request.user.username)
    saldo = user.usersaldo.saldo
    context = {'supplier_list': supplier_list,
               'user': request.user.username,
               'saldo': str(saldo)}
    return render(request, 'kasse/supplier.html', context)


@login_required
def Statistics(request):
    order_list = Orders.objects.order_by('-start_date')[:30]
    user = User.objects.get(username=request.user.username)
    saldo = user.usersaldo.saldo
    topten = Supplier.objects.annotate(
        num_orders=Count('orders')).order_by('-num_orders')[:10]
    context = {'order_list': order_list,
               'user': request.user.username,
               'saldo': str(saldo),
               'topten': topten}
    return render(request, 'kasse/statistics.html', context)


@login_required
def Orderdetail(request, pk):
    order = get_object_or_404(Orders, pk=pk)
    orderitems = OrderItems.objects.all().filter(order=order.id)
    summary = OrderItems.objects.all().filter(order=order).aggregate(Sum('value'))
    user = User.objects.get(username=request.user.username)
    saldo = user.usersaldo.saldo
    context = {'order': order,
               'orderitems': orderitems,
               'summary': summary['value__sum'],
               'user': request.user.username,
               'saldo': str(saldo)}
    return render(request, 'kasse/orderdetails.html', context)


@login_required
def Myorders(request):
    user = User.objects.get(username=request.user.username)
    saldo = user.usersaldo.saldo
    orderitems = OrderItems.objects.all().filter(user=user.id)
    context = {'orderitems': orderitems,
               'user': request.user.username,
               'saldo': str(saldo)}
    return render(request, 'kasse/myorders.html', context)


@login_required
def Accounting(request):
    user = User.objects.get(username=request.user.username)
    saldo = user.usersaldo.saldo
    deposits = Deposit.objects.all().filter(user=user).order_by('-deposit_date')
    context = {'user': request.user.username,
               'saldo': str(saldo),
               'deposits': deposits}
    return render(request, 'kasse/accounting.html', context)


@login_required
def deposit(request):
    user = User.objects.get(username=request.user.username)
    value = float(request.POST.get("deposit_value").replace(',', '.'))
    try:
        Deposit.objects.create(user=user, value=value)
    except Exception as e:
        print(e)
    return HttpResponseRedirect(reverse('kasse:accounting'))
