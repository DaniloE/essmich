from django.contrib import admin
from .models import Supplier, Orders, Deposit, DiscountRules


class DiscountInline(admin.TabularInline):
    model = DiscountRules
    extra = 2


class OrderAdmin(admin.ModelAdmin):
    list_display = ('supplier', 'start_date',
                    'end_date', 'active', 'deliverytime')
    list_filter = ['active']
    readonly_fields = ['deactivate_date']
    fieldsets = (
        (None, {
            'fields': ('supplier', 'end_date', 'deliverytime', 'active')
        }),
        ('Automated Values', {
            'classes': ('collapse',),
            'fields': ('start_date', 'deactivate_date'),
        }),
    )

    # more readonly fields in edit than in create (edit=obj exists, create=object is none)
    def get_readonly_fields(self, request, obj=None):
        if obj:
            if not obj.active:  # more restrictions if order is already closed
                return self.readonly_fields + ['supplier', 'start_date', 'active', 'end_date']
            else:
                return self.readonly_fields + ['supplier', 'start_date']
        return self.readonly_fields


class DepositAdmin(admin.ModelAdmin):
    list_display = ('user', 'value', 'deposit_date',
                    'accepted_date', 'accepted')
    list_filter = ['accepted', 'user']
    search_fields = ['user']
    exclude = ('accepted_date',)
    readonly_fields = ['accepted_date', 'deposit_date', 'user', 'value']

    def has_add_permission(self, request, obj=None):
        return False


class SupplierAdmin(admin.ModelAdmin):
    inlines = [DiscountInline]


admin.site.register(Supplier, SupplierAdmin)
admin.site.register(Orders, OrderAdmin)
admin.site.register(Deposit, DepositAdmin)
